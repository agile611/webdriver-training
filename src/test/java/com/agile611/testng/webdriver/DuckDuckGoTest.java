package com.agile611.testng.webdriver;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.*;

import java.io.File;
import java.util.concurrent.TimeUnit;


public class DuckDuckGoTest {

    public WebDriver driver;
    public static JavascriptExecutor jse;

    @BeforeClass(alwaysRun = true) //Inicialización del navegador
    public void setUp() throws Exception {
        DesiredCapabilities capabilities;
        capabilities = DesiredCapabilities.chrome();
        System.setProperty("webdriver.chrome.driver", "src" + File.separator + "main" + File.separator + "resources" + File.separator + "chromedriver-macos");
        driver = new ChromeDriver(capabilities);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        jse = (JavascriptExecutor) driver;
    }

    @AfterClass(alwaysRun = true) //El cierre del navegador
    public void tearDown() throws Exception {
        driver.quit();
    }

    @Test //El propio test que hemos exportado de Katalon IDE
    public void testUntitledTestCase() throws Exception {
        driver.get("https://www.duckduckgo.com");
    }
}